(ns game-of-life.core
  (:gen-class)
  (:use [clojure.string :only [join]])
  (:use [clojure.pprint :only [pprint]]))

(defn generateInitialState []
  [[0 0 0 0 0 0 0 0 1 0]
   [0 0 0 0 0 0 0 1 0 0]
   [0 0 0 0 0 0 0 1 1 1]
   [0 0 0 0 0 0 0 0 0 0]
   [0 0 0 0 0 0 0 0 0 0]
   [0 0 0 0 0 0 0 0 0 0]
   [0 0 0 0 0 0 0 0 0 0]
   [0 0 0 0 0 0 0 0 0 0]
   [0 0 0 0 0 0 0 0 0 0]
   [0 0 0 0 0 0 0 0 0 0]])

(defn neighbors [[i j]]
  (vec (for [row [-1 0 1] col (if (= row 0) [-1 1] [-1 0 1])]
         [(+ i row) (+ j col)])))

(defn getState [world [i j]]
  (let [row (count world)
        col (-> world first count)]
    (if (and (>= i 0) (< i row) (>= j 0) (< j col)) ((world i) j)
      0)))

(defn countAliveNeighbors [neighbors world]
  ((frequencies (map (partial getState world)  neighbors)) 1))

(defn step [world]
  (vec (for [i (range 0 (count world))]
    (vec (for [j (range 0 (-> world first count))]
      (let [alive (countAliveNeighbors (neighbors [i j]) world)]
        (cond (= alive 3) 1
              (= alive 2) ((world i) j)
              :default 0)))))))

(defn stepper [n]
  (defn innerStepper [cnt world]
    (pprint world)
    (if (> cnt 0)
      (recur (dec cnt) (step world))))
  (innerStepper n (generateInitialState)))

(defn -main
  ([] (pprint (stepper 1)))
  ([numberOfGenerations] (pprint (stepper (read-string numberOfGenerations)))))
